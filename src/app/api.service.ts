import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public apicontrie = "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php";
  public  urlapi:string='https://coronavirus-tracker-api.herokuapp.com/v2/locations';
  public urlapi2="https://coronavirus-monitor.p.rapidapi.com/coronavirus/worldstat.php";
  public appkey="1f7c1b8817mshee90c7dfe202eb7p15cff0jsn016fc8bbb9d2";
  public rapidapurl="coronavirus-monitor.p.rapidapi.com";
  constructor(private http:HttpClient) { }
  getinfo() {
    console.log("0000000");
    return this.http.get(this.urlapi,{headers: new HttpHeaders({'content-type': "application/json" })});
  }
  getgobal() {
    console.log("0000000");
    return this.http.get(this.urlapi2,{headers: new HttpHeaders({'x-rapidapi-host': this.rapidapurl ,'x-rapidapi-key': this.appkey,'content-type': "application/json" })});
  }

  getcontrie() {
    console.log("0000000");
    return this.http.get(this.apicontrie,{headers: new HttpHeaders({'x-rapidapi-host': this.rapidapurl ,'x-rapidapi-key': this.appkey,'content-type': "application/json" })});
  }
}
