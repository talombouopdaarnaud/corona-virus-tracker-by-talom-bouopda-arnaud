import { Component,OnInit } from '@angular/core';
import {ApiService} from "../api.service";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page  implements OnInit{

  tab:any=[];
  public camp: any=[];
  public global: any;
  searchCountry: any;
  public load2 :number=1;
  constructor(private U:ApiService) {}
  ngOnInit() {
    this.getdata();
  }
  afficherbilan(){
 
    this.U.getinfo()
    .subscribe(data => {
      
      this.tab=data;
      this.global = this.tab.latest;

      this.tab.locations.forEach(c=>{
        console.log(c);
        this.camp.push(c);
  
      });
      if(this.camp){
        this.load2=2;
      }
  
     
  
  
     // loading.dismiss();
  
    }, error => {
      this.load2=3;
    });
   
  
    }
    getdata(){
 
      this.U.getcontrie()
      .subscribe(data => {
        
        this.tab=data;
        this.global = this.tab.countries_stat;
  
        this.tab.countries_stat.forEach(c=>{
          console.log(c);
          this.camp.push(c);
    
        });
        if(this.camp){
          this.load2=2;
        }
    
       
    
    
       // loading.dismiss();
    
      }, error => {
        this.load2=3;
      });
     
    
      }

}
